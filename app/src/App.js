import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Dashboard from './components/pages/dashboard/Dashbaord';
import ProjectDetails from './components/pages/projects/ProjectDetails';
import SignIn from './components/pages/auth/SignIn';
import SignUp from './components/pages/auth/SignUp';
import CreateProject from './components/pages/projects/CreateProject';
// Reduc
import { Provider } from 'react-redux';
import store from './redux/store';

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<BrowserRouter>
					<Navbar></Navbar>
					<Switch>
						<Route exact path="/auth/signin" component={SignIn}></Route>
						<Route exact path="/auth/signup" component={SignUp}></Route>

						<Route exact path="/" component={Dashboard}></Route>
						<Route exact path="/projects/new" component={CreateProject}></Route>
						<Route exact path="/projects/:id" component={ProjectDetails}></Route>
					</Switch>
				</BrowserRouter>
			</Provider>
		);
	}
}

export default App;
