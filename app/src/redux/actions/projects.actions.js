import { ADD_PROJECT, ADD_PROJECT_ERROR } from '../types';
import { firebase, db } from '../../config/firebase';

/**
 * Add a new project
 */
export const addProject = (project) => async (dispatch) => {
	// make async call to database
	try {
		await db.collection('projects').add({
			...project, // title, content
			author: {
				id: '12345',
				firstName: 'Tobias',
				lastName: 'Wälde'
			},
			createdAt: firebase.firestore.FieldValue.serverTimestamp()
		});
		dispatch({ type: ADD_PROJECT, payload: project });
	} catch (error) {
		dispatch({ type: ADD_PROJECT_ERROR, payload: error });
	}
};
