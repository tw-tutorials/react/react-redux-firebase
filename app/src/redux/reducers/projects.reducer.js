import { ADD_PROJECT, ADD_PROJECT_ERROR } from '../types';

const initialState = {
	projects: [
		{ id: '1', title: 'Project 1', content: 'Some content for project 1' },
		{ id: '2', title: 'Project 2', content: 'Some content for project 2' },
		{ id: '3', title: 'Project 3', content: 'Some content for project 3' },
		{ id: '4', title: 'Project 4', content: 'Some content for project 4' }
	]
};

export default function(state = initialState, action) {
	switch (action.type) {
		case ADD_PROJECT:
			console.log('created project', action.payload);
			return state;

		case ADD_PROJECT_ERROR:
			console.log('create project error', action.payload);
			return state;

		default:
			return state;
	}
}
