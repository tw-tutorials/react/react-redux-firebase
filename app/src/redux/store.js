import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
// Middleware
import thunk from 'redux-thunk';

// Reducers
import authReducer from './reducers/auth.reducer';
import projectsReducer from './reducers/projects.reducer';

const reducers = combineReducers({
	auth: authReducer,
	projects: projectsReducer
});

// Initial state
const initialState = {};

// Enhancers
const composeEnhancers =
	typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
		? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
		: compose;
const middleware = [thunk];
const enhancers = composeEnhancers(applyMiddleware(...middleware));

// Create store
const store = createStore(reducers, initialState, enhancers);
export default store;
