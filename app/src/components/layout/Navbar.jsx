import React from 'react';
import { Link } from 'react-router-dom';
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import { version } from '../../../package.json';

const Navbar = () => {
	return (
		<nav className="nav-wrapper grey darken-3">
			<div className="container">
				<Link to="/" className="brand-logo left">
					MarioPlan
				</Link>
				<SignedInLinks></SignedInLinks>
				<SignedOutLinks></SignedOutLinks>
				<ul className="right">
					<li>
						<span className="grey-text">{version}</span>
					</li>
				</ul>
			</div>
		</nav>
	);
};

export default Navbar;
