import React from 'react';
import { NavLink } from 'react-router-dom';

const SignedOutLinks = () => {
	return (
		<ul className="right">
			<li>
				<NavLink to="/auth/signup">Sign Up</NavLink>
			</li>
			<li>
				<NavLink to="/auth/signin">Sign In</NavLink>
			</li>
		</ul>
	);
};

export default SignedOutLinks;
