import React, { Component } from 'react';

class SignUp extends Component {
	state = {
		data: {
			firstName: '',
			lastName: '',
			email: '',
			password: ''
		}
	};

	handleChange = ({ currentTarget }) => {
		let { data } = this.state;
		data[currentTarget.id] = currentTarget.value;
		this.setState({ data });
	};
	handleSubmit = (e) => {
		e.preventDefault();

		console.log(this.state.data);
	};

	render() {
		return (
			<div className="container">
				<form className="white" autoComplete="yes" onSubmit={this.handleSubmit}>
					<h5 className="grey-text text-darken-3">Sign Up</h5>
					<div className="input-field">
						<label htmlFor="firstName">First name</label>
						<input
							type="text"
							id="firstName"
							name="fname"
							autoComplete="fname"
							onChange={this.handleChange}
						></input>
					</div>
					<div className="input-field">
						<label htmlFor="lastName">Last name</label>
						<input
							type="text"
							id="lastName"
							name="lname"
							autoComplete="lname"
							onChange={this.handleChange}
						></input>
					</div>
					<div className="input-field">
						<label htmlFor="email">Email</label>
						<input
							type="email"
							id="email"
							name="email"
							autoComplete="email"
							onChange={this.handleChange}
						></input>
					</div>
					<div className="input-field">
						<label htmlFor="password">Password</label>
						<input
							type="password"
							id="password"
							name="password"
							autoComplete="password"
							onChange={this.handleChange}
						></input>
					</div>
					<div className="input-field">
						<button className="btn pink lighten-1">Sign Up</button>
					</div>
				</form>
			</div>
		);
	}
}

export default SignUp;
