import React, { Component } from 'react';

class SignIn extends Component {
	state = {
		data: {
			email: '',
			password: ''
		}
	};

	handleChange = ({ currentTarget }) => {
		let { data } = this.state;
		data[currentTarget.id] = currentTarget.value;
		this.setState({ data });
	};
	handleSubmit = (e) => {
		e.preventDefault();

		console.log(this.state.data);
	};

	render() {
		return (
			<div className="container">
				<form className="white" onSubmit={this.handleSubmit}>
					<h5 className="grey-text text-darken-3">Sign In</h5>
					<div className="input-field">
						<label htmlFor="email">Email</label>
						<input type="email" id="email" onChange={this.handleChange}></input>
					</div>
					<div className="input-field">
						<label htmlFor="password">Password</label>
						<input type="password" id="password" onChange={this.handleChange}></input>
					</div>
					<div className="input-field">
						<button className="btn pink lighten-1">Login</button>
					</div>
				</form>
			</div>
		);
	}
}

export default SignIn;
