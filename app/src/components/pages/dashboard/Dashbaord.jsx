import React, { Component } from 'react';
import Notifications from './Notifications';
import ProjectList from '../projects/ProjectList';
// Redux
import { connect } from 'react-redux';

class Dashboard extends Component {
	render() {
		const {
			projects: { projects }
		} = this.props;

		return (
			<div className="dashboard container">
				<div className="row">
					<div className="col s12 m6">
						<ProjectList projects={projects}></ProjectList>
					</div>
					<div className="col s12 m5 offset-m1">
						<Notifications></Notifications>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	projects: state.projects
});

const mapDispatchToProps = {};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Dashboard);
