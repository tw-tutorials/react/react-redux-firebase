import React from 'react';

const ProjectDetails = (props) => {
	const id = props.match.params.id;

	return (
		<div className="container section project-details">
			<div className="card">
				<div className="card-content">
					<span className="card-title">Project Title - {id}</span>
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam iusto
						voluptatibus doloremque sapiente laboriosam ipsam praesentium? Doloribus
						expedita ad, temporibus illo aspernatur fugit placeat tenetur, quia, ducimus
						quaerat assumenda aperiam?
					</p>
				</div>
				<div className="card-action grey lighten-4 grey-text">
					<div>Posted by Tobias Wälde</div>
					<div>20th Nov, 9pm</div>
				</div>
			</div>
		</div>
	);
};

export default ProjectDetails;
