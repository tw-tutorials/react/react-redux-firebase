import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
import { addProject } from '../../../redux/actions/projects.actions';

class CreateProject extends Component {
	state = {
		data: {
			title: '',
			content: ''
		}
	};

	handleChange = ({ currentTarget }) => {
		let { data } = this.state;
		data[currentTarget.id] = currentTarget.value;
		this.setState({ data });
	};
	handleSubmit = (e) => {
		e.preventDefault();
		this.props.addProject(this.state.data);
	};

	render() {
		return (
			<div className="container">
				<form className="white" onSubmit={this.handleSubmit}>
					<h5 className="grey-text text-darken-3">Create new project</h5>
					<div className="input-field">
						<label htmlFor="title">Title</label>
						<input type="text" id="title" onChange={this.handleChange}></input>
					</div>
					<div className="input-field">
						<label htmlFor="content">Project Content</label>
						<textarea
							id="content"
							className="materialize-textarea"
							onChange={this.handleChange}
						></textarea>
					</div>
					<div className="input-field">
						<button className="btn pink lighten-1">Create</button>
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
	addProject
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CreateProject);
