import React from 'react';

const ProjectSummary = ({ project }) => {
	return (
		<div className="card project-summary">
			<div className="card-content grey-text text-darken-3">
				<span className="card-title">{project.title}</span>
				<p>Posted by Tobias Wälde</p>
				<p className="grey-text">20th Nov, 9pm</p>
			</div>
		</div>
	);
};

export default ProjectSummary;
