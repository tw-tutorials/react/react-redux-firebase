import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import { firebaseConfig } from './firebaseConfig';

// Initialize firebase
firebase.initializeApp(firebaseConfig);

// Initialize firestore
// firebase.firestore().settings({ timestampsInSnapshots: true }); //? deprecated
const db = firebase.firestore();

export { firebase, db };
